﻿namespace CloudWatchLogger
{
    public class EventLog
    {
        public string Event { get; set; }
        public double Value { get; set; }

        public static EventLog Heartbeat => new EventLog
        {
            Event = "Heartbeat",
            Value = 1
        };
    }
}
