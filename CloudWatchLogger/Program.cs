﻿using System.Threading.Tasks;
using Amazon;
using Amazon.CloudWatchLogs;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;
using Serilog.Formatting.Compact;
using Serilog.Sinks.AwsCloudWatch;

namespace CloudWatchLogger
{
    class Program
    {
        public static AppConfig Settings { get; set; }

        static async Task Main(string[] args)
        {
            var hostBuilder = new HostBuilder()
                .ConfigureAppConfiguration(builder =>
                {
                    builder.AddJsonFile("appsettings.json", false);
                })
                .ConfigureServices((hostContext, services) =>
                {
                    Settings = new AppConfig();
                    hostContext.Configuration.Bind("AppSettings", Settings);
                    services.AddSingleton<IHostedService, FakeService>();
                })
                .UseSerilog((hostContext, loggerConfig) =>
                {
                    var settings = new CloudWatchSinkOptions
                    {
                        LogGroupName = Settings.CloudWatchLogGroupName,
                        LogStreamNameProvider = new ConstantLogStreamNameProvider(Settings.CloudWatchLogStreamNamePrefix),
                        TextFormatter = new CompactJsonFormatter()
                    };

                    var client = new AmazonCloudWatchLogsClient(RegionEndpoint.USWest2);

                    loggerConfig
                        .MinimumLevel.Debug()
                        .WriteTo.Logger(l => l
                            .Filter.ByIncludingOnly(Settings.ConsoleFilter)
                            .WriteTo.Console())
                        .WriteTo.Logger(l => l
                            .Filter.ByExcluding(Settings.SeqFilter)
                            .WriteTo.Seq(Settings.SeqUrl, apiKey: Settings.SeqKey))
                        .WriteTo.Logger(l => l
                            .Filter.ByIncludingOnly(Settings.CloudWatchFilter)
                            .WriteTo.AmazonCloudWatch(settings, client)
                        );
                })
                .Build();

            using (hostBuilder)
            {
                await hostBuilder.StartAsync();
                await hostBuilder.StopAsync();
            }
        }
    }
}
