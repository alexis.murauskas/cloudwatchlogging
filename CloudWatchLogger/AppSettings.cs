﻿namespace CloudWatchLogger
{
    public class AppConfig
    {
        public string CloudWatchLogGroupName { get; set; }
        public string CloudWatchLogStreamNamePrefix { get; set; }
        public string SeqUrl { get; set; }
        public string SeqKey { get; set; }
        public string ConsoleFilter { get; set; }
        public string SeqFilter { get; set; }
        public string CloudWatchFilter { get; set; }
    }
}
