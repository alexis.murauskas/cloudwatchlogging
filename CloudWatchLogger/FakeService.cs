﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace CloudWatchLogger
{
    public class FakeService : IHostedService
    {
        private readonly ILogger<FakeService> _logger;

        public FakeService(ILogger<FakeService> logger)
        {
            _logger = logger;
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            var startTime = DateTime.UtcNow;
            _logger.LogDebug("Program start: " + startTime);

            try
            {
                await ThrowsException();
            }
            catch (Exception e)
            {
                _logger.LogError("Exception thrown: {@Exception}", e);
            }

            _logger.LogWarning("WARNING!");
            _logger.LogCritical("Critical system failure!");

            var heartbeat = EventLog.Heartbeat;
            _logger.LogInformation("Metric log: {@EventLog}", heartbeat);

            var runTime = DateTime.UtcNow - startTime;
            _logger.LogDebug("Program run time: " + runTime);
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }

        private async Task ThrowsException()
        {
            throw new ArgumentNullException();
        }
    }
}
